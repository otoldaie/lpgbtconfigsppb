#felix_host=felixstripserver
felix_host=pcatlitkflx03
#felix_host=pcatlitkflx04
stave=$1
speed=$2

if echo "$stave" | grep -q "B"
then
    #PPB1B
    echo configuring sec lpGBT on dev 0 link 1
    python3 confSeclpGBT_PPB.py --host ${felix_host} --device 0 --link 1 --lpGBT_config ~/config/lpgbtConfigsPPB/${speed}Mbps/lpgbt_SS_M_LH_Sec_${speed}.cnf
    echo configuring sec lpGBT on dev 0 link 3
    python3 confSeclpGBT_PPB.py --host ${felix_host} --device 0 --link 3 --lpGBT_config ~/config/lpgbtConfigsPPB/${speed}Mbps/lpgbt_SS_S_LH_Sec_${speed}.cnf
fi

if echo "$stave" | grep -q "A"
then
    #PPB1a
    echo configuring sec lpGBT on dev 0 link 5
    python3 confSeclpGBT_PPB.py --host ${felix_host} --device 0 --link 5 --lpGBT_config ~/config/lpgbtConfigsPPB/${speed}Mbps/lpgbt_SS_S_LH_Sec_${speed}.cnf
    echo configuring sec lpGBT on dev 0 link 7
    python3 confSeclpGBT_PPB.py --host ${felix_host} --device 0 --link 7 --lpGBT_config ~/config/lpgbtConfigsPPB/${speed}Mbps/lpgbt_SS_M_LH_Sec_${speed}.cnf
fi

if echo "$stave" | grep -q "C"
then
    #PPB1C
    echo configuring sec lpGBT on dev 0 link 8
    python3 confSeclpGBT_PPB.py --host ${felix_host} --device 0 --link 8 --lpGBT_config ~/config/lpgbtConfigsPPB/${speed}Mbps/lpgbt_SS_M_LH_Sec_${speed}.cnf
    echo configuring sec lpGBT on dev 0 link 10
    python3 confSeclpGBT_PPB.py --host ${felix_host} --device 0 --link 10 --lpGBT_config ~/config/lpgbtConfigsPPB/${speed}Mbps/lpgbt_SS_S_LH_Sec_${speed}.cnf
fi

