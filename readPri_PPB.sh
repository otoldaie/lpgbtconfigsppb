felix_host=pcatlitkflx03
stave=$1

if echo "$stave" | grep -q "B"
then
    #PPB1B
    echo reading primary lpGBT on link 1
    ic_over_netio -f ${felix_host} -T 12340 -r 12350 -i 0x71 -e $((0x1d + 0x40*1 )) -x $((0x15 + 0x40*1)) --read
    echo reading primary lpGBT on link 3
    ic_over_netio -f ${felix_host} -T 12340 -r 12350 -i 0x71 -e $((0x1d + 0x40*3 )) -x $((0x15 + 0x40*3)) --read
fi

if echo "$stave" | grep -q "A"
then
    #PPB1A
    echo reading primary lpGBT on link 5
    ic_over_netio -f ${felix_host} -T 12340 -r 12350 -i 0x71 -e $((0x1d + 0x40*5 )) -x $((0x15 + 0x40*5)) --read
    echo reading primary lpGBT on link 7
    ic_over_netio -f ${felix_host} -T 12340 -r 12350 -i 0x71 -e $((0x1d + 0x40*7 )) -x $((0x15 + 0x40*7)) --read
fi

if echo "$stave" | grep -q "C"
then
    #PPB1C
    echo reading primary lpGBT on link 8
    ic_over_netio -f ${felix_host} -T 12340 -r 12350 -i 0x71 -e $((0x1d + 0x40*8 )) -x $((0x15 + 0x40*8)) --read
    echo reading primary lpGBT on link 10
    ic_over_netio -f ${felix_host} -T 12340 -r 12350 -i 0x71 -e $((0x1d + 0x40*10 )) -x $((0x15 + 0x40*10)) --read
fi

if echo "$stave" | grep -q "D"
then
    #PPB1D
    echo reading primary lpGBT on link 14
    ic_over_netio -f ${felix_host} -T 12341 -r 12351 -i 0x71 -e $((0x1d + 0x40*2 + 0x800 )) -x $((0x15 + 0x40*2 + 0x800)) --read
    echo reading primary lpGBT on link 15
    ic_over_netio -f ${felix_host} -T 12341 -r 12351 -i 0x71 -e $((0x1d + 0x40*3 + 0x800)) -x $((0x15 + 0x40*3 + 0x800)) --read
fi

echo done!
