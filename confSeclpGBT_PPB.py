import pydcs
import argparse
import logging
import sys

ap = argparse.ArgumentParser()
ap.add_argument("-host", "--hostname",type=str,help='host name where to run ic_over_netio command')
ap.add_argument("-l", "--link", default=-1, type=int, help='optical link on FLX device to connect to')
ap.add_argument("--tx-shift",  type=int, default=0, help='the shift of the TX optical link wrt RX, default=0, set to -1 if needed')
ap.add_argument("-rx", "--toHost",type=int,help='to Host link for ic_over_netio command')
ap.add_argument("-tx", "--fromHost",type=int,help='from Host link for ic_over_netio command')
ap.add_argument("--device",default= 0, type=int, help='device to run on')
ap.add_argument("--lpGBT_config",type=str,help='lpGBT config')
ap.add_argument('-m', "--sec-i2c-master", type=int, default=2, help='the i2c master node number (0, 1, 2) that is connected to the secondary lpGBT (2 on PPB)')
ap.add_argument("--vtrx-i2c-master", type=int, default=1, help='the i2c master node number (0, 1, 2) that is connected to the VTRX (1 on PPB)')
ap.add_argument("--sec-lpgbt-i2c-addr", type=int, default=0x70, help='the i2c address of the secondary lpGBT (default 0x70)')

ap.add_argument('-d', "--debug", action='store_true', help='set logging level to DEBUG')

args = ap.parse_args()

if args.debug:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

# secondary lpGBT should be: "lpgbt_SS_S_LH_Sec_toTry.cnf"
# primary lpGBT should be: "lpgbt_SS_M_LH_Sec_toTry.cnf"

if args.device == 0:
    tx_port = 12340
    rx_port = 12350
    elink_offset = 0

if args.device == 1:
    tx_port = 12341
    rx_port = 12351
    elink_offset = 0x800

elink_rx, elink_tx = None, None

if args.link != -1:
    elink_rx = 0x40 * args.link + 0x1d
    elink_tx = 0x40 * args.link + 0x15
    #if args.tx_shift and elink_tx >= 0x40:
    if (elink_tx + args.tx_shift * 0x40) < 0:
        logging.error(f"the tx link shift {args.tx_shift} makes the tx link number negative, exiting")
        exit(2)
    else:
        elink_tx += args.tx_shift * 0x40
# direct rx-tx elinks take precedence over overall optical link
if args.toHost:   elink_rx = args.toHost
if args.fromHost: elink_tx = args.fromHost

if elink_rx is None or elink_tx is None:
    raise Exception(f"either RX or TX elinks are not defined: {elink_rx} {elink_tx}")

# ic-over-netio connection to the primary lpgbt
elink_rx += elink_offset
elink_tx += elink_offset

logging.debug(f"lpgbt connection: {args.hostname} txport={tx_port} rxport={rx_port} rxelink={elink_rx} txelink={elink_tx} i2caddr=0x71")
l = pydcs.lpGBT(args.hostname, tx_port, rx_port, elink_rx, elink_tx, 0x71, pydcs.IC_DEV_lpGBT, False)

# sanity checks
# read the primary lpgbt chipid registers
logging.debug(l.readRegs([0x000, 0x001, 0x002, 0x003]))
# read the VTRX version
logging.debug(f'l.lpgbtVTRXReadSingle({args.vtrx_i2c_master}, 0x15) = {l.lpgbtVTRXReadSingle(args.vtrx_i2c_master, 0x15)}')

# read the secondary lpGBT read-only registers: test and i2c address
i2c_master = args.sec_i2c_master
logging.debug(f'l.lpgbtSecReadBlock({i2c_master}, {args.sec_lpgbt_i2c_addr}, 0x1c5, 1) = {l.lpgbtSecReadBlock(i2c_master, args.sec_lpgbt_i2c_addr, 0x1c5, 1)}')
logging.debug(f'l.lpgbtSecReadBlock({i2c_master}, {args.sec_lpgbt_i2c_addr}, 0x141, 1) = {l.lpgbtSecReadBlock(i2c_master, args.sec_lpgbt_i2c_addr, 0x141, 1)}')

#logging.debug("read the config RW registers of the primary lpgbt")
#cfg_main = l.readRegs(range(316))
reg_addresses = range(316)

logging.debug(f"read the config file for the secondary lpGBT: {args.lpGBT_config}")
with open(args.lpGBT_config) as file_in:
    lines = []
    for line in file_in:
        lines.append(int(line.replace('\n',''), 16))

logging.info(f"write the config file to the secondary lpGBT")
n_progress_steps = len(reg_addresses)
n_progress_bar_chars = 30
for i, reg_addr in enumerate(reg_addresses):
    reg_val = lines[reg_addr]
    # print(reg_addr, reg_val)
    l.lpgbtSecWriteSingle(i2c_master, args.sec_lpgbt_i2c_addr, reg_addr, reg_val)

    # the progress bar
    j = (i + 1) / n_progress_steps # current progress fraction
    # draw the progress bar only if it has to be redrawn
    progress_chars_to_draw = int(n_progress_bar_chars * j)
    progress_chars_prev    = int(n_progress_bar_chars * (i) / n_progress_steps)

    # print the progress bar:
    if progress_chars_to_draw > progress_chars_prev:
        _ = sys.stdout.write(f"\r[%-{n_progress_bar_chars}s] %d%%" % ('='*progress_chars_to_draw, 100*j))
        sys.stdout.flush()

print(' done') # also prints the newline

# confirm the written config
#cfg_test = l.lpgbtSecReadBlock(i2c_master, 0x70, 0x0, 317)
#assert cfg_test == lines
